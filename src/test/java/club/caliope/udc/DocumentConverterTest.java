/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
package club.caliope.udc;

import club.caliope.udc.InputFormat;
import club.caliope.udc.Settings;
import club.caliope.udc.OutputFormat;
import club.caliope.udc.DocumentConverter;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.UncheckedIOException;
import org.junit.After;
import org.junit.Test;
import org.junit.Before;
import static org.junit.Assert.*;

public class DocumentConverterTest {

    private DocumentConverter instance;

    private final File fromOdtFile = new File("test-docs/Con los ojos cerrados.odt");
    private final File fromDocxFile = new File("test-docs/Con los ojos cerrados.docx");
    private final File toAsciiFile = new File("test-docs/result.txt");
    private final File toHtmlFile = new File("test-docs/result.html");

    @Before
    public void setup() {
        instance = new DocumentConverter();
    }

    @After
    public void cleanup() {
        toAsciiFile.delete();
        toHtmlFile.delete();
    }
    
    @Test
    public void convert_docxToAscii_createsFile() throws IOException {
        instance.fromFile(fromDocxFile, InputFormat.DOCX)
                .toFile(toAsciiFile, OutputFormat.ASCIIDOC)
                .convert();

        assertTrue(toAsciiFile.isFile());
        assertTrue(toAsciiFile.length() > 11000);
        assertTrue(toAsciiFile.length() < 12000);
        assertFirstLineEquals(toAsciiFile, "Con los ojos cerrados");
    }    

    @Test
    public void convert_odtToAscii_createsFile() throws IOException {
        instance.fromFile(fromOdtFile, InputFormat.ODT)
                .toFile(toAsciiFile, OutputFormat.ASCIIDOC)
                .convert();

        assertTrue(toAsciiFile.isFile());
        assertTrue(toAsciiFile.length() > 11000);
        assertTrue(toAsciiFile.length() < 12000);
        assertFirstLineEquals(toAsciiFile, "Con los ojos cerrados");
    }
    
    @Test
    public void convert_odtToHtml_createsFile() throws IOException {
        instance.fromFile(fromOdtFile, InputFormat.ODT)
                .toFile(toHtmlFile, OutputFormat.HTML5)
                .convert();

        assertTrue(toHtmlFile.isFile());
        assertTrue(toHtmlFile.length() > 11000);
        assertTrue(toHtmlFile.length() < 12000);
        assertFirstLineEquals(toHtmlFile, "<p>Con los ojos cerrados</p>");
    }    

    @Test
    public void convert_odtToHtmlWithStandaloneOption_createsFile() throws IOException {
        instance.fromFile(fromOdtFile, InputFormat.ODT)
                .toFile(toHtmlFile, OutputFormat.HTML5)
                .addOption("-s")
                .convert();

        assertTrue(toHtmlFile.isFile());
        assertTrue(toHtmlFile.length() > 12000);
        assertTrue(toHtmlFile.length() < 13000);
        assertFirstLineEquals(toHtmlFile, "<!DOCTYPE html>");
    }

    @Test(expected = UncheckedIOException.class)
    public void convert_pandocIsNotInstalled_throwsException() {
        Settings settings = new Settings();
        settings.setPandocExec("pandoc_exec_that_does_not_exist.exe");
        instance = new DocumentConverter(settings);

        instance.fromFile(fromOdtFile, InputFormat.ODT)
                .toFile(toAsciiFile, OutputFormat.ASCIIDOC)
                .convert();
    }

    @Test(expected = RuntimeException.class)
    public void convert_inputFileDoesNotExist_throwsException() {
        instance.fromFile(new File("file_that_does_not_exist.odt"), InputFormat.ODT)
                .toFile(toAsciiFile, OutputFormat.ASCIIDOC)
                .convert();
    }

    /**
     * Asserts if the first line in the file is equals to the given parameter.
     */
    private void assertFirstLineEquals(File file, String expected) {
        try (BufferedReader br = new BufferedReader(new FileReader(file))) {
            String firstLine = br.readLine();
            assertEquals(expected, firstLine);
        } catch (IOException ex) {
            fail("Could not read file");
            throw new UncheckedIOException(ex);
        }
    }

}
